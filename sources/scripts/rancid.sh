#!/bin/bash


if [ "$EUID" -ne 0 ];
    then echo "Please run as root"
    exit
fi

echo "Remember to install svn, perl, expect, gnudiff, tcl"
folder=$( ls /opt | grep ranc | wc -l)
if [ $folder != "0" ]
    then echo "rancid is already installed"
else
    curl -O ftp://ftp.shrubbery.net/pub/rancid/rancid-3.1.tar.gz 
    tar -xvzf rancid-3.1.tar.gz -C /opt/ 
    rm -f rancid-3.1.tar.gz
    echo "rancid 3.1 has been installed in /opt/rancid-3.1"
fi
