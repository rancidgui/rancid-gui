#!/bin/bash

if  [ -z $1 ] 
then echo -e "BAD USAGE: Enter the version of rancid\n -help for more information" 
exit 0
fi 

if [ $1 = "-help" ]
then 
echo -e "RANCID DEPLOY SCRIPT

Created by Yoshi and JoeKuro 


USAGE:

deploy_rancid.sh <-help>
"| less  
fi



if perl < /dev/null > /dev/null 2>&1
then
echo "ok"

else

echo -e "Perl not found\n Installing perl\n"
# Installer script for Perl on Linux/Unix systems

INSTALLER_PERL_VERSION=5.20.1

BASHR=~/.bashrc
CPANMTMP=~/.cpanm
PBREW_BASHRC=~/perl5/perlbrew/etc/bashrc

# See if 'make' is installed
if [ "" == "$(which 'make')" ]; then
	echo "Unable to find 'make' please install"
	exit;
fi;

# Ask them to clean ~/.cpanm - so we don't have file perm issues
if [ -d "$CPANMTMP" ]; then
	echo "------------------------"
	echo "Please delete $CPANMTMP and then re-run this command"
	echo "You may need to run 'sudo rm -rf $CPANMTMP'"
	echo "if you ran cpanm with sudo"
	exit;
fi

echo "Installing perlbrew"
if [ "x${PERLBREWURL}" == "x" ]; then
    PERLBREWURL=http://gugod.github.io/App-perlbrew/perlbrew
fi

if [ -z "$TMPDIR" ]; then
    if [ -d "/tmp" ]; then
        TMPDIR="/tmp"
    else
        TMPDIR="."
    fi
fi

cd $TMPDIR || exit 1

LOCALINSTALLER="perlbrew-$$"

echo
if type curl >/dev/null 2>&1; then
  PERLBREWDOWNLOAD="curl -f -k -sS -Lo $LOCALINSTALLER $PERLBREWURL"
elif type fetch >/dev/null 2>&1; then
  PERLBREWDOWNLOAD="fetch --no-verify-peer -o $LOCALINSTALLER $PERLBREWURL"
elif type wget >/dev/null 2>&1; then
  PERLBREWDOWNLOAD="wget -nv --no-check-certificate -O $LOCALINSTALLER $PERLBREWURL"
else
  echo "Need wget or curl to use $0"
  exit 1
fi

clean_exit () {
  [ -f $LOCALINSTALLER ] && rm $LOCALINSTALLER
  exit $1
}

echo "## Download the latest perlbrew"
$PERLBREWDOWNLOAD || clean_exit 1

echo
echo "## Installing perlbrew"

PERL="/usr/bin/perl"
[ ! -x "$PERL" ] && PERL="/usr/local/bin/perl"

if [ ! -x "$PERL" ]; then
  echo "Need /usr/bin/perl or /usr/local/bin/perl to use $0"
  clean_exit 1
fi

chmod +x $LOCALINSTALLER
$PERL $LOCALINSTALLER self-install || clean_exit 1

echo "## Installing patchperl"
$PERL $LOCALINSTALLER -f -q install-patchperl || clean_exit 1

echo
echo "## Done."
rm ./$LOCALINSTALLER


echo "Checking/updating $BASHR to source perlbrew bashrc"
if [ ! -f $BASHR ]; then
	# File missing so create
	echo "source $PBREW_BASHRC" >> $BASHR
else
	if [ "" == "$( cat $BASHR | grep 'source ' | grep $PBREW_BASHRC )" ]; then
		# source line is missing - so add
		echo "source $PBREW_BASHRC" >> $BASHR
	fi;
fi;

echo "Updating your current environment"
source $PBREW_BASHRC

echo "Installing Perl $INSTALLER_PERL_VERSION through perlbrew"
perlbrew -n install perl-$INSTALLER_PERL_VERSION

echo "Setting Perl $INSTALLER_PERL_VERSION to default"
perlbrew switch perl-$INSTALLER_PERL_VERSION

echo "Installing cpanm",
perlbrew install-cpanm

echo "------------------"
echo "Install complete - close this terminal window and open a new one,"
echo "then to confirm Perl $INSTALLER_PERL_VERSION is installed type: perl -v"
fi


#after checked installing rancind
# wget ftp://ftp.shrubbery.net/pub/rancid/rancid-3.1.tar.gz
# tar -zxvf rancid-3.1.tar.gz /opt/
# svn tcl cvs gnudiff
#
















